var algorithm = (function () {

    return {floydWarshall: floydWarshall};

    // JS Infinite hesaplama problemi yuzunden bunu yapmak zorunda kaldim :(
    var infinite = 111111111;

    function init() {
        var graph = getMatrixData();

        for (var i = 0; i < graph.length; i++) {
            for (var j = 0; j < graph.length; j++) {
                if (i === j)
                    graph[i][j] = 0;
                else if (graph[i][j] == "NaN" || isNaN(graph[i][j]))
                    graph[i][j] = infinite;
                else if (graph[i][j] == "") {
                    graph[i][j] = infinite;
                }
            }
        }
        return graph;
    }

    function getMatrixData() {
        var graph = [];
        expression = /td_([0-9]+)_([0-9]+)/;
        $(".input").each(function () {
            matched = expression.exec($(this).attr("name"));
            if (graph[matched[1]] == null) {
                graph[matched[1]] = [];
            }
            graph[matched[1]][matched[2]] = $("input[name='" + matched[0] + "']").val();
        });

        return graph;
    }

    function floydWarshall() {
        graph = init();
        for (var k = 0; k < graph.length; k++) {
            for (var i = 0; i < graph.length; i++) {
                for (var j = 0; j < graph.length; j++) {
                    if (parseInt(graph[i][j]) > (parseInt(graph[i][k]) + parseInt(graph[k][j]))) {
                        graph[i][j] = parseInt(graph[i][k]) + parseInt(graph[k][j]);
                    }

                }
            }
        }
        printToTable(graph);
    }

    function printToTable(graph) {
        alert("Hesaplandı!");
        for (var i = 0; i < graph.length; i++) {
            for (var j = 0; j < graph[i].length; j++) {
                $("input[name=td_" + i + "_" + j).val(graph[i][j]);
            }
        }
    }

}());


function createMatrix(size) {
    if (size > 10) {
        alert("Matris boyutu maksimum 10 olabilir!");
        return;
    }

    if (size < 2) {
        alert("Matris boyutu minimum 2 olabilir!");
        return;
    }


    var table = createTable(size);
    $("#matrixForm").html(table);
    $("#calculateButton").removeAttr("style");
}

function createTable(size) {
    var table = "<table><tr><td></td>";
    for (var i = 0; i < size; i++) {
        table = table.concat("<td><center>i" + i + "</center></td>");
    }
    table = table.concat("</tr><tr>")
    for (var i = 0; i < size; i++) {
        table = table.concat("<td>j" + i + "</td>");
        table = table.concat();
        for (var j = 0; j < size; j++) {
            if (i == j) {
                table = table.concat("<td><input type='text' name='td_" + i + "_" + j + "' value='0' disabled='disabled' size='3' class='input'/></td>");
                continue;
            }
            table = table.concat("<td><input type='text' name='td_" + i + "_" + j + "' size='3' maxlength='3' class='input'/></td>");
        }
        table = table.concat("</tr>");
    }
    table = table.concat("</table>");

    return table;
}